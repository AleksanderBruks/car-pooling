package ba.basic.helloworld;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ba.basic.helloworld.dao.UserDAO;
import ba.basic.helloworld.dao.UserEntity;

@Entity
@Service
@Transactional
public class UserManagerImpl implements UserManager{
	
	@ManyToOne
	@Autowired
    private UserDAO userDAO;

	@Override
	@Transactional
	public void addUser(UserEntity travel) {
		userDAO.addUser(travel);
	}
	
	@Override
	@Transactional
	public void deleteUser(Integer travelId) {
		userDAO.deleteUser(travelId);
	}
	
	public void setTravelDAO(UserDAO userDAO) {
	    this.userDAO = userDAO;
	}

	@Override
	public UserEntity getUser(String email) {
		// TODO Auto-generated method stub
		return userDAO.getUser(email);	
	}

}
