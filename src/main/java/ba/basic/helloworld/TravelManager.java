package ba.basic.helloworld;

import java.util.List;

import ba.basic.helloworld.dao.ReservedEntity;
import ba.basic.helloworld.dao.TravelEntity;
 
public interface TravelManager {
    public void addTravel(TravelEntity travel);
    public List<TravelEntity> getAllTravels();
    public void deleteTravel(Integer travelId);
    public List<TravelEntity> searchTravels(TravelEntity travel);
    public void updateTravel(TravelEntity travel);
    public TravelEntity getTravel(Integer travelId);
    public List<TravelEntity> quickSearchTravels(Integer driverId, String start, String end);
    
    
	public void addReserved(ReservedEntity reserved);
	public List<ReservedEntity> getReserved(Integer userId);
}