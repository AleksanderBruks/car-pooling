package ba.basic.helloworld;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ba.basic.helloworld.dao.FavouriteDAO;
import ba.basic.helloworld.dao.FavouriteEntity;
import ba.basic.helloworld.dao.TravelDAO;

@Entity
@Service
@Transactional
public class FavouriteManagerImpl implements FavouriteManager {

    @ManyToOne
	@Autowired
        private FavouriteDAO favouriteDAO;
	
	@Override
	public void addFavourite(FavouriteEntity favourite) {
		favouriteDAO.addFavourite(favourite);

	}

	@Override
	public void deleteFavourite(Integer favouriteId) {
		favouriteDAO.deleteFavourite(favouriteId);
	}

	@Override
	@Transactional
	public List<FavouriteEntity> getFavourites(Integer owner) {
		return favouriteDAO.getFavourites(owner);
	}
	@Override
	public FavouriteEntity getFavourite (Integer favouriteId){
		return favouriteDAO.getFavourite(favouriteId);
	}

}
