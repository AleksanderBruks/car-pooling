package ba.basic.helloworld;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ba.basic.helloworld.dao.CommentEntity;
import ba.basic.helloworld.dao.RatingEntity;
import ba.basic.helloworld.dao.UserProfileDAO;
import ba.basic.helloworld.dao.UserProfileEntity;

@Entity
@Service
@Transactional
public class UserProfileManagerImpl implements UserProfileManager {
	
	@ManyToOne
	@Autowired
	private UserProfileDAO userProfileDao;
	
	@Override
    @Transactional
	public void addUserProfile(UserProfileEntity userProfile) {
		userProfileDao.addUserProfile(userProfile);
		
	}

	@Override
    @Transactional
	public List<UserProfileEntity> getAllProfiles() {
		return userProfileDao.getAllProfiles();
	}

	@Override
    @Transactional
	public void deleteUserProfile(Integer userProfileId) {
		userProfileDao.deleteUserProfile(userProfileId);	
	}

	public void setUserProfileDao(UserProfileDAO userProfileDao) {
		this.userProfileDao = userProfileDao;
	}

	@Override
	public UserProfileEntity getProfile(Integer userProfileId) {
		return this.userProfileDao.getUserProfile(userProfileId);
	}

	@Override
	@Transactional
	public void updateProfile(UserProfileEntity userProfile) {
		userProfileDao.updateProfile(userProfile);
	}

	@Override
	@Transactional
	public void addRating(RatingEntity rating) {
		userProfileDao.addRating(rating);
		
	}

	@Override
	@Transactional
	public void updateRating(RatingEntity rating) {
		userProfileDao.updateRating(rating);
		
	}

	@Override
	public RatingEntity getRating(Integer userId) {
		return userProfileDao.getRating(userId);
	}

	@Override
	public void addComment(CommentEntity comment) {
		userProfileDao.addComment(comment);
	}

	@Override
	public List<CommentEntity> getAllComments(Integer userId) {
		return userProfileDao.getAllComments(userId);
		
	}

}
