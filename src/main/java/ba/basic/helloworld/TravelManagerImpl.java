package ba.basic.helloworld;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ba.basic.helloworld.dao.ReservedDAO;
import ba.basic.helloworld.dao.ReservedEntity;
import ba.basic.helloworld.dao.TravelDAO;
import ba.basic.helloworld.dao.TravelEntity;
 
 
@Entity
@Service
@Transactional
public class TravelManagerImpl implements TravelManager {
 
    @ManyToOne
	@Autowired
        private TravelDAO travelDAO;
    
    @ManyToOne
    @Autowired
    	private ReservedDAO reservedDAO;
 
    @Override
    @Transactional
    public void addTravel(TravelEntity travel) {
        travelDAO.addTravel(travel);
    }
 
    @Override
    @Transactional
    public List<TravelEntity> getAllTravels() {
        return travelDAO.getAllTravels();
    }
 
    @Override
    @Transactional
    public void deleteTravel(Integer travelId) {
        travelDAO.deleteTravel(travelId);
    }
 
    public void setTravelDAO(TravelDAO travelDAO) {
        this.travelDAO = travelDAO;
    }

	@Override
	public List<TravelEntity> searchTravels(TravelEntity travel) {
		return travelDAO.searchTravels(travel);
	}

	@Override
	public void addReserved(ReservedEntity reserved) {
		reservedDAO.addReserved(reserved);
		
	}

	@Override
	public List<ReservedEntity> getReserved(Integer userId) {
		return reservedDAO.getReserved(userId);
	}
    @Override
    @Transactional
    public void updateTravel(TravelEntity travel){
    	travelDAO.updateTravel(travel);
    };
    @Override
    public TravelEntity getTravel(Integer travelId){
    	return travelDAO.getTravel(travelId);
    	
    };
    @Override
    public List<TravelEntity> quickSearchTravels(Integer driverId, String start, String end) {
    	return travelDAO.quickSearchTravels(driverId, start, end);
    }
}