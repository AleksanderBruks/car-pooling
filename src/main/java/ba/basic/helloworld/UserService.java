package ba.basic.helloworld;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ba.basic.helloworld.dao.UserDAO;
import ba.basic.helloworld.dao.UserEntity;

@Entity
@Service
@Transactional
public class UserService implements UserDetailsService {

	@ManyToOne
	@Autowired
	private UserDAO userDAO;

	public void setUserDao(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Transactional
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		// load user
		UserEntity user = userDAO.getUser(username);

		if (user != null) {

			// convert roles
			List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
			// for (Privilege p : user.getPrivileges()) {
			roles.add(new SimpleGrantedAuthority("ROLE_USER"));
			// }

			// initialize user
			SecurityUser securityUser = new SecurityUser(user.getEmail(),
					user.getPassword(), true, true, true, true, roles);

			securityUser.setUser(user);

			return securityUser;
		} else {
			throw new UsernameNotFoundException("No user with username '"
					+ username + "' found!");
		}
	}

}
