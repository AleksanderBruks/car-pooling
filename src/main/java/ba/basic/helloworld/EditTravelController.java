package ba.basic.helloworld;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ba.basic.helloworld.dao.CommentEntity;
import ba.basic.helloworld.dao.FavouriteEntity;
import ba.basic.helloworld.dao.RatingEntity;
import ba.basic.helloworld.dao.ReservedEntity;
import ba.basic.helloworld.dao.TravelEntity;
import ba.basic.helloworld.dao.UserEntity;
import ba.basic.helloworld.dao.UserProfileEntity;

 
@Entity
@Controller
public class EditTravelController {
 
    @ManyToOne
	@Autowired
    private TravelManager travelManager;
    
    @ManyToOne
	@Autowired
    private UserManager userManager;
    
    @ManyToOne
	@Autowired
    private UserProfileManager userProfileManager;
    
    @ManyToOne
 	@Autowired
     private FavouriteManager favouriteManager;
 
    public void setTravelManager(TravelManager travelManager) {
        this.travelManager = travelManager;
    }
    
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
    
    public void setUserProfileManager(UserProfileManager userProfileManager) {
        this.userProfileManager = userProfileManager;
    }
    ////////////////////////////////////////////////////////////////////////////////////////        search    ////////////////////////////////
    @RequestMapping(value = { "/search"}, method = RequestMethod.GET)
    public String search(ModelMap map, HttpServletRequest request)
    {
    	map.addAttribute("travel", new TravelEntity()); 
		return "search";
    }
    
    @RequestMapping(value = { "/searchRoute"}, method = RequestMethod.POST)
    public String searchRoute(@ModelAttribute(value="travel") TravelEntity travel, BindingResult result, ModelMap map)
    {
    	 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
         String name = auth.getName(); //get logged in username
         UserEntity currentUser = userManager.getUser(name);
         travel.setWho(currentUser.getId());
         List<TravelEntity> allTravels = travelManager.searchTravels(travel);
         
         Map<Integer, UserProfileEntity> drivers = new HashMap<Integer, UserProfileEntity>();
         if(allTravels!=null){
	         for(TravelEntity t:allTravels){
	        
	         	drivers.put(t.getWho(), userProfileManager.getProfile(t.getWho()));//drivers.add(f);
	         }
         }
         map.addAttribute("travelsList", allTravels);
         map.addAttribute("drivers",drivers);

         return "editTravelsList";
    }
    
    @RequestMapping(value = { "/quickSearch/{favouriteId}"}, method = RequestMethod.GET)
    public String quickSearch(ModelMap map, @PathVariable("favouriteId") Integer favouriteId){
        FavouriteEntity f = favouriteManager.getFavourite(favouriteId);
        Integer driver;
        
        if (f.getDriver()==null)
        	driver=0;
        else
        	driver=f.getDriver();
        
        List<TravelEntity> allTravels = travelManager.quickSearchTravels(driver, f.getFrom(), f.getTo());
        Map<Integer, UserProfileEntity> drivers = new HashMap<Integer, UserProfileEntity>();
        if(allTravels!=null){
	        for(TravelEntity t:allTravels){
	        	drivers.put(t.getWho(), userProfileManager.getProfile(t.getWho()));//drivers.add(f);
	        }
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        UserEntity currentUser = userManager.getUser(name);
        List<FavouriteEntity> allFavourites= favouriteManager.getFavourites(currentUser.getId());
        // List<FavouriteEntity> drivers = new ArrayList<FavouriteEntity>();
         List<FavouriteEntity> routes = new ArrayList<FavouriteEntity>();
         Map<Integer, UserProfileEntity> favouriteDrivers = new HashMap<Integer, UserProfileEntity>();
         for(FavouriteEntity fav:allFavourites){
         	if(fav.getType().equals("driver")) favouriteDrivers.put(fav.getId(), userProfileManager.getProfile(fav.getDriver()));//drivers.add(f);
         	if(fav.getType().equals("route")) routes.add(fav);
         }
     	map.addAttribute("favouriteDrivers", favouriteDrivers);
     	map.addAttribute("routes", routes);
         
         map.addAttribute("travelsList", allTravels);
         map.addAttribute("drivers",drivers);
  
         return "editTravelsList";
    }

    
    ////////////////////////////////////////////////////////////////////////////////////////       profiles    //////////////////////////////// 
    @RequestMapping(value = { "/myProfile"}, method = RequestMethod.GET)
    public String showMyProfile(ModelMap map){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        UserEntity currentUser = userManager.getUser(name);
        UserProfileEntity currentProfile=userProfileManager.getProfile(currentUser.getId());
        RatingEntity rating =userProfileManager.getRating(currentUser.getId());
        List<CommentEntity> comments = userProfileManager.getAllComments(currentUser.getId());
           
        map.addAttribute("profile", currentProfile);   	
    	map.addAttribute("rating", rating);
    	map.addAttribute("comments",comments);
       // map.addAttribute("travelsList", travelManager.getAllTravels());
		return "myProfile";	
    }
    
    @RequestMapping(value = "/addProfile", method = RequestMethod.POST)
    public String addProfile(@ModelAttribute(value="profile") UserProfileEntity profile, BindingResult result)
    {
    	userProfileManager.updateProfile(profile);
        // travelManager.addTravel(travel);
        return "redirect:/myProfile";
    }
    
    @RequestMapping(value = "/userProfile/addComment", method = RequestMethod.POST)
    public String addComment(@ModelAttribute(value="newComment") CommentEntity newComment, BindingResult result, HttpServletRequest request)
    {
    	userProfileManager.addComment(newComment);
    	String referer = request.getHeader("Referer");
		return "redirect:"+ referer;
    }
    
    @RequestMapping(value = { "/userProfile/{userId}"}, method = RequestMethod.GET)
    public String showProfile(ModelMap map, @PathVariable("userId") Integer userId){
        UserProfileEntity userProfile=userProfileManager.getProfile(userId);
        RatingEntity rating =userProfileManager.getRating(userId);
        List<CommentEntity> comments = userProfileManager.getAllComments(userId);
        CommentEntity newComment = new CommentEntity();
        newComment.setUserId(userId);
    	map.addAttribute("profile", userProfile);
    	map.addAttribute("rating", rating);
    	map.addAttribute("comments",comments);
    	map.addAttribute("newComment", newComment);
		return "userProfile";	
    }
    @RequestMapping(value = { "/userProfile/increaseRating/{userId}"}, method = RequestMethod.GET)
    public String increaseRating(ModelMap map, @PathVariable("userId") Integer userId, HttpServletRequest request)
    {
    	RatingEntity rating = userProfileManager.getRating(userId);
    	rating.setTotal(rating.getTotal()+1);
    	userProfileManager.updateRating(rating);
    	String referer = request.getHeader("Referer");
		return "redirect:"+ referer;
    }
    @RequestMapping(value = { "/userProfile/decreaseRating/{userId}"}, method = RequestMethod.GET)
    public String decreaseRating(ModelMap map, @PathVariable("userId") Integer userId, HttpServletRequest request)
    {
    	RatingEntity rating = userProfileManager.getRating(userId);
    	rating.setTotal(rating.getTotal()-1);
    	userProfileManager.updateRating(rating);
    	String referer = request.getHeader("Referer");
		return "redirect:"+ referer;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////       favourites    //////////////////////////////// 
    @RequestMapping(value = "/favourites", method = RequestMethod.GET)
    public String favourites(ModelMap model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        UserEntity currentUser = userManager.getUser(name);
        List<FavouriteEntity> allFavourites= favouriteManager.getFavourites(currentUser.getId());
       // List<FavouriteEntity> drivers = new ArrayList<FavouriteEntity>();
        List<FavouriteEntity> routes = new ArrayList<FavouriteEntity>();
        Map<Integer, UserProfileEntity> drivers = new HashMap<Integer, UserProfileEntity>();
        for(FavouriteEntity f:allFavourites){
        	if(f.getType().equals("driver")) drivers.put(f.getId(), userProfileManager.getProfile(f.getDriver()));//drivers.add(f);
        	if(f.getType().equals("route")) routes.add(f);
        }
        model.addAttribute("newRoute", new FavouriteEntity());
    	model.addAttribute("drivers", drivers);
    	model.addAttribute("routes", routes);
    	return "favourites";
    }
    
    
    @RequestMapping("/deleteFavourite/{favouriteId}")
    public String deleteFavourite(@PathVariable("favouriteId") Integer favouriteId)
    {
        favouriteManager.deleteFavourite(favouriteId);
        return "redirect:/favourites";
    }
    
    @RequestMapping(value="/userProfile/addFavouriteDriver/{userId}",  method=RequestMethod.GET)
    public String addFavouriteDriver(@ModelAttribute(value="favourite") FavouriteEntity favourite, BindingResult result, @PathVariable("userId") Integer userId, HttpServletRequest request)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        UserEntity currentUser = userManager.getUser(name);
        List<FavouriteEntity> allFavourites= favouriteManager.getFavourites(currentUser.getId());
    	
    	favourite.setType("driver");
    	favourite.setDriver(userId);
        favourite.setOwner(currentUser.getId());
        
        
        for(FavouriteEntity f:allFavourites){
        	if(f.getType().equals(favourite.getType())){
        		if(f.getOwner()==favourite.getOwner() && f.getDriver()==favourite.getDriver() && f.getType().equals(favourite.getType())){
        			String referer = request.getHeader("Referer");
        			return "redirect:"+ referer;
        		}
        	}
        }
        
        favouriteManager.addFavourite(favourite);
        return "redirect:/favourites";
    }
    
    @RequestMapping(value="/addFavouriteRoute",  method=RequestMethod.POST)
    public String addFavouriteRoute(@ModelAttribute(value="newRoute") FavouriteEntity newRoute, BindingResult result, HttpServletRequest request)
    {	
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        UserEntity currentUser = userManager.getUser(name);
        List<FavouriteEntity> allFavourites= favouriteManager.getFavourites(currentUser.getId());
    	
        newRoute.setType("route");
        newRoute.setOwner(currentUser.getId());
        System.out.println(newRoute.getFrom()+"   "+newRoute.getTo());
        for(FavouriteEntity f:allFavourites){
        	if(f.getType().equals(newRoute.getType())){
        		if(f.getOwner()==newRoute.getOwner() && f.getFrom().equals(newRoute.getFrom()) && f.getTo().equals(newRoute.getTo())){
        			String referer = request.getHeader("Referer");
        			return "redirect:"+ referer;
        		}
        	}
        }
       
        favouriteManager.addFavourite(newRoute);
        return "redirect:/favourites";
    }
//////////////////////////////////////////////////////////////////////////////////////                travels    //////////////////////////////// 
    @RequestMapping(value = { "/home", "/" }, method = RequestMethod.GET)
    public String listTravels(ModelMap map)
    {
    	List<TravelEntity> allTravels=travelManager.getAllTravels();
        Map<Integer, UserProfileEntity> drivers = new HashMap<Integer, UserProfileEntity>();
        if(allTravels!=null){
	        for(TravelEntity t:allTravels){
	        	drivers.put(t.getWho(), userProfileManager.getProfile(t.getWho()));//drivers.add(f);
	        }
        }
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        UserEntity currentUser = userManager.getUser(name);
        List<FavouriteEntity> allFavourites= favouriteManager.getFavourites(currentUser.getId());
       // List<FavouriteEntity> drivers = new ArrayList<FavouriteEntity>();
        List<FavouriteEntity> routes = new ArrayList<FavouriteEntity>();
        Map<Integer, UserProfileEntity> favouriteDrivers = new HashMap<Integer, UserProfileEntity>();
        for(FavouriteEntity f:allFavourites){
        	if(f.getType().equals("driver")) favouriteDrivers.put(f.getId(), userProfileManager.getProfile(f.getDriver()));//drivers.add(f);
        	if(f.getType().equals("route")) routes.add(f);
        }
    	map.addAttribute("favouriteDrivers", favouriteDrivers);
    	map.addAttribute("routes", routes);
        
        map.addAttribute("travelsList", allTravels);
        map.addAttribute("drivers",drivers);
 
        return "editTravelsList";
       // return "redirect:/login";
    }
    
    @RequestMapping(value = { "/announcement" }, method = RequestMethod.GET)
    public String newAnnouncement(ModelMap map)
    {
    	
        map.addAttribute("travel", new TravelEntity()); 
        return "announcement";
       // return "redirect:/login";
    }
    
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addTravel(@ModelAttribute(value="travel") TravelEntity travel, BindingResult result)
    {
    	 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
         String name = auth.getName(); //get logged in username
         UserEntity currentUser = userManager.getUser(name);
         travel.setWho(currentUser.getId());
        travelManager.addTravel(travel);
        return "redirect:/home";
    }
 
    @RequestMapping("/delete/{travelId}")
    public String deleteTravel(@PathVariable("travelId") Integer travelId)
    {
        travelManager.deleteTravel(travelId);
        return "redirect:/home";
    }
    @RequestMapping("/addReserved/{travelId}")
    public String addReserved(@PathVariable("travelId") Integer travelId, ModelMap map){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        UserEntity currentUser = userManager.getUser(name);
        ReservedEntity newReserved = new ReservedEntity();
        newReserved.setPassengerId(currentUser.getId());
        newReserved.setTravelId(travelId);
        travelManager.addReserved(newReserved);
        TravelEntity travel = travelManager.getTravel(travelId);
        travel.setFreePlaces(travel.getFreePlaces()-1);
      //  travelManager.updateTravel(travel);
        List<ReservedEntity> allReserved= travelManager.getReserved(currentUser.getId());
        map.addAttribute("allReserved",allReserved);
        return "redirect:/myReserved";
    }
    @RequestMapping("/myReserved")
    public String myReserved(ModelMap map){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        UserEntity currentUser = userManager.getUser(name);
        List<ReservedEntity> allReserved= travelManager.getReserved(currentUser.getId());
        HashMap<Integer, TravelEntity> trav = new HashMap<Integer, TravelEntity>();
        HashMap<Integer, UserProfileEntity> drivers = new HashMap<Integer, UserProfileEntity>();
        for (ReservedEntity r: allReserved){
        	TravelEntity t =travelManager.getTravel(r.getTravelId());
        	trav.put(r.getId(), t);
        	drivers.put(r.getId(), userProfileManager.getProfile(t.getWho()));
        	System.out.println(userProfileManager.getProfile(t.getWho()).getFirstName());
        }
        map.addAttribute("allReserved", allReserved);
        map.addAttribute("travels", trav);
        map.addAttribute("drivers",drivers);
        
    	return "reserved";
    }
 
    
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @RequestMapping(value = "/quickNav", method = RequestMethod.GET)
    public String quickNav(ModelMap model) {	
    	 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
         String name = auth.getName(); //get logged in username
         UserEntity currentUser = userManager.getUser(name);
         List<FavouriteEntity> allFavourites= favouriteManager.getFavourites(currentUser.getId());
        // List<FavouriteEntity> drivers = new ArrayList<FavouriteEntity>();
         List<FavouriteEntity> routes = new ArrayList<FavouriteEntity>();
         Map<Integer, UserProfileEntity> drivers = new HashMap<Integer, UserProfileEntity>();
         for(FavouriteEntity f:allFavourites){
         	if(f.getType().equals("driver")) drivers.put(f.getId(), userProfileManager.getProfile(f.getDriver()));//drivers.add(f);
         	if(f.getType().equals("route")) routes.add(f);
         }
        model.addAttribute("newRoute", new FavouriteEntity());
     	model.addAttribute("drivers", drivers);
     	model.addAttribute("routes", routes);
        return "quickNav";
    }
    
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap model) {
        return "login";
    }
    
    @RequestMapping(value = "/regulations", method = RequestMethod.GET)
    public String regulations(ModelMap model) {
        return "regulations";
    }
    
 
    @RequestMapping(value = "/accessdenied", method = RequestMethod.GET)
    public String loginerror(ModelMap model) {
        model.addAttribute("error", "true");
        return "denied";
    }
 
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(ModelMap model) {
        return "logout";
    }

    
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String createUser(@ModelAttribute(value="user") UserEntity user, BindingResult result) {
    	UserProfileEntity userProfile = new UserProfileEntity();
    	RatingEntity rating = new RatingEntity();
    	
    	user.setPassword(md5(user.getPassword()));
    	user.setRole("user");
    	userManager.addUser(user);
    	UserEntity newUser = userManager.getUser(user.getEmail());
    	userProfile.setUserId(newUser.getId());
    	userProfileManager.addUserProfile(userProfile);
    	rating.setUserId(newUser.getId());
    	rating.setTotal(0);
    	userProfileManager.addRating(rating);
        return "redirect:/login";
    }
    
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(ModelMap model) {
    	UserEntity userForm = new UserEntity();    
        model.put("userForm", userForm);
        return "register";
    }
    
    private String md5(String plainText)
    {
    	
    	MessageDigest mDigest;
		try {
			mDigest = MessageDigest.getInstance("MD5");
		
	        byte[] result = mDigest.digest(plainText.getBytes());
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < result.length; i++) {
	            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			return plainText;
		}
    }
}
