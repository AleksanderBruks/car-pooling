package ba.basic.helloworld;

import java.util.List;

import ba.basic.helloworld.dao.FavouriteEntity;

public interface FavouriteManager {
	public void addFavourite(FavouriteEntity favourite);
	public void deleteFavourite(Integer favouriteId);
	public List<FavouriteEntity> getFavourites(Integer owner);
	public FavouriteEntity getFavourite (Integer favouriteId);
}
