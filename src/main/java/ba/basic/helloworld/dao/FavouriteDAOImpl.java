package ba.basic.helloworld.dao;

import java.util.List;

import javax.persistence.Entity;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Entity
@Repository
public class FavouriteDAOImpl implements FavouriteDAO {
	

    @Autowired
        private SessionFactory sessionFactory;

	@Override
	public void addFavourite(FavouriteEntity favourite) {
		 this.sessionFactory.getCurrentSession().save(favourite);

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FavouriteEntity> getFavourites(Integer userId) {
		List<FavouriteEntity> favourites = this.sessionFactory.getCurrentSession()
				.createQuery("from FavouriteEntity where owner = ?")
				.setParameter(0, userId).list();
	return favourites;
	}

	@Override
	public void deleteFavourite(Integer favouritelId) {
		FavouriteEntity favourite = (FavouriteEntity) sessionFactory.getCurrentSession().load(
				FavouriteEntity.class, favouritelId);
	        if (null != favourite) {
	            this.sessionFactory.getCurrentSession().delete(favourite);
	        }
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public FavouriteEntity getFavourite (Integer favouriteId) {
		List<FavouriteEntity> favourites = this.sessionFactory.getCurrentSession()
				.createQuery("from FavouriteEntity where id = ?")
				.setParameter(0, favouriteId).list();
	return favourites.get(0);
	}

}
