package ba.basic.helloworld.dao;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "reserved")
public class ReservedEntity {

		
		@Id
		@Column(name = "id")
		@GeneratedValue
		private Integer id;

		
		@Column(name = "passenger_id")
		private Integer passengerId;

		@Column(name = "travel_id")
		private Integer travelId;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getPassengerId() {
			return passengerId;
		}

		public void setPassengerId(Integer passengerId) {
			this.passengerId = passengerId;
		}

		public Integer getTravelId() {
			return travelId;
		}

		public void setTravelId(Integer travelId) {
			this.travelId = travelId;
		}

	}

