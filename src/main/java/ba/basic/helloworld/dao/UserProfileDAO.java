package ba.basic.helloworld.dao;

import java.util.List;

public interface UserProfileDAO 
{
    public void addUserProfile(UserProfileEntity userProfile);
    public List<UserProfileEntity> getAllProfiles();
    public void deleteUserProfile(Integer userProfileId);
    public UserProfileEntity getUserProfile(Integer userProfileId);
    public void updateProfile(UserProfileEntity userProfile);
    
    public void addRating(RatingEntity rating);
    public void updateRating(RatingEntity rating);
    public RatingEntity getRating(Integer userId);
    
    public void addComment(CommentEntity comment);
    public List<CommentEntity> getAllComments(Integer userId);
}
