package ba.basic.helloworld.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "favourites")
public class FavouriteEntity {

		@Id
		@Column(name = "id")
		@GeneratedValue
		private Integer id;

		@Column(name = "owner")
		private Integer owner;
		
		@Column(name="type")
		private String type;
		
		@Column(name="driver")
		private Integer driver;
		
		@Column(name="start")
		private String from;
		
		@Column(name="end")
		private String to;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getOwner() {
			return owner;
		}

		public void setOwner(Integer owner) {
			this.owner = owner;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public Integer getDriver() {
			return driver;
		}

		public void setDriver(Integer driver) {
			this.driver = driver;
		}

		public String getFrom() {
			return from;
		}

		public void setFrom(String from) {
			this.from = from;
		}

		public String getTo() {
			return to;
		}

		public void setTo(String to) {
			this.to = to;
		}

}
