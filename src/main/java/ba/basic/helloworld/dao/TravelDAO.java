package ba.basic.helloworld.dao;

import java.util.List;
 
public interface TravelDAO
{
    public void addTravel(TravelEntity travel);
    public List<TravelEntity> getAllTravels();
    public void deleteTravel(Integer travelId);
    public List<TravelEntity> searchTravels(TravelEntity travel);
    public void updateTravel(TravelEntity travel);
    public TravelEntity getTravel(Integer travelId);
    public List<TravelEntity> quickSearchTravels(Integer driverId, String start, String end);
}