package ba.basic.helloworld.dao;

import java.util.List;

import javax.persistence.Entity;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
 
@Entity
@Repository
public class TravelDaoImpl implements TravelDAO  {
 
    @Autowired
        private SessionFactory sessionFactory;
 
    @Override
    public void addTravel(TravelEntity travel) {
        this.sessionFactory.getCurrentSession().save(travel);
    }
 
    @SuppressWarnings("unchecked")
    @Override
    public List<TravelEntity> getAllTravels() {
        return this.sessionFactory.getCurrentSession().createQuery("from TravelEntity where free_places > 0 order by begin_date DESC").list();
    }
 
    @Override
    public void deleteTravel(Integer travelId) {
        TravelEntity travel = (TravelEntity) sessionFactory.getCurrentSession().load(
                TravelEntity.class, travelId);
        if (null != travel) {
            this.sessionFactory.getCurrentSession().delete(travel);
        }
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<TravelEntity> searchTravels(TravelEntity travel) {
		List<TravelEntity> travels = this.sessionFactory.getCurrentSession()
				.createQuery("from TravelEntity where start = ? and to = ? order by begin_date DESC")
				.setParameter(0, travel.getFrom())
				.setParameter(1, travel.getTo())
				.list();
	return travels == null || travels.size() <= 0 ? null : travels;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TravelEntity> quickSearchTravels(Integer driverId, String start, String end) {
		List<TravelEntity> travels = this.sessionFactory.getCurrentSession()
				.createQuery("from TravelEntity where who = ? or ( start = ? and to = ? ) order by begin_date DESC")
				.setParameter(0,driverId)
				.setParameter(1, start)
				.setParameter(2, end)
				.list();
	return travels == null || travels.size() <= 0 ? null : travels;
	}
	 @Override
	 public TravelEntity getTravel(Integer travelId){
		 @SuppressWarnings("unchecked")
		List <TravelEntity> travel = this.sessionFactory.getCurrentSession()
					.createQuery("from TravelEntity where id = ?")
					.setParameter(0, travelId).list();
		return travel == null || travel.size() <= 0 ? null : (TravelEntity) travel.get(0);
	 }
	
	@Override
	@Transactional
	public void updateTravel(TravelEntity travel) {
		this.sessionFactory.getCurrentSession().update(travel);
	}
}
