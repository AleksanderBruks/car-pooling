package ba.basic.helloworld.dao;

import java.util.List;

import javax.persistence.Entity;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Entity
@Repository
public class ReservedDAOImpl implements ReservedDAO {
	
    @Autowired
    private SessionFactory sessionFactory;

	@Override
	public void addReserved(ReservedEntity reserved) {
		 this.sessionFactory.getCurrentSession().save(reserved);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ReservedEntity> getReserved(Integer userId) {
		List<ReservedEntity> reserved = this.sessionFactory.getCurrentSession()
				.createQuery("from ReservedEntity where passengerId = ?")
				.setParameter(0, userId).list();
	return reserved;
	}

}
