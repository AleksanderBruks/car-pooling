package ba.basic.helloworld.dao;

public interface UserDAO {
	
	public void addUser(UserEntity user);
	public UserEntity getUser(String username);
	public void deleteUser(Integer userId);
}
