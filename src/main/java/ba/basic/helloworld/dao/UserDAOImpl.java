package ba.basic.helloworld.dao;

import java.util.List;

import javax.persistence.Entity;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Entity
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addUser(UserEntity user) {
		this.sessionFactory.getCurrentSession().save(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public UserEntity getUser(String email) {
		//System.out.println("ODCZYTUJE USERAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		List<UserEntity> users = this.sessionFactory.getCurrentSession()
				.createQuery("from UserEntity where email = ?")
				.setParameter(0, email).list();
		System.out.println(users.get(0).getEmail());
		return users == null || users.size() <= 0 ? null : (UserEntity) users
				.get(0);
	}

	@Override
	public void deleteUser(Integer userId) {
		UserEntity user = (UserEntity) sessionFactory.getCurrentSession().load(
				UserEntity.class, userId);
		if (null != user) {
			this.sessionFactory.getCurrentSession().delete(user);
		}
	}
}
