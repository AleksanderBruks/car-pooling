package ba.basic.helloworld.dao;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table(name="TRAVEL")
public class TravelEntity {
 
    @Id
    @Column(name="id")
    @GeneratedValue
    private Integer id;

	@Column(name="who")
    private Integer who;
 
    @Column(name="start")
    private String from;
    
    @Column(name="end")
    private String to;

    @Column(name="free_places")
    private Integer freePlaces;
    
    @Column(name="begin_date")
    private String when;
    
    @Column(name="cost")
    private Integer cost;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWho() {
		return who;
	}

	public void setWho(Integer who) {
		this.who = who;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Integer getFreePlaces() {
		return freePlaces;
	}

	public void setFreePlaces(Integer freePlaces) {
		this.freePlaces = freePlaces;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public String getWhen() {
		return when;
	}

	public void setWhen(String when) {
		this.when = when;
	}
    


}