package ba.basic.helloworld.dao;

import java.util.List;

public interface ReservedDAO {

	public void addReserved(ReservedEntity reserved);
	public List<ReservedEntity> getReserved(Integer userId);
}
