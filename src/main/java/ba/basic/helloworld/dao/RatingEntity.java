package ba.basic.helloworld.dao;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ratings")
public class RatingEntity {
	
	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;

	@Column(name = "user_id")
	private Integer userId;

	@Column(name = "total")
	private Integer total;

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
