package ba.basic.helloworld.dao;

import java.util.List;

public interface FavouriteDAO {
    public void addFavourite(FavouriteEntity favourite);
    public List<FavouriteEntity> getFavourites(Integer userId);
    public void deleteFavourite(Integer favouritelId);
    public FavouriteEntity getFavourite (Integer favouriteId);
}
