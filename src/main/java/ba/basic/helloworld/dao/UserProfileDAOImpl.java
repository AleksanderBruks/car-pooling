package ba.basic.helloworld.dao;

import java.util.List;

import javax.persistence.Entity;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Entity
@Repository
public class UserProfileDAOImpl implements UserProfileDAO {

    @Autowired
        private SessionFactory sessionFactory;
 
    @Override
	public void addUserProfile(UserProfileEntity userProfile) {
    	 this.sessionFactory.getCurrentSession().save(userProfile);
	}
    
    @SuppressWarnings("unchecked")
    @Override
	public List<UserProfileEntity> getAllProfiles() {
    	return this.sessionFactory.getCurrentSession().createQuery("from UserProfileEntity").list();
	}

	@Override
	@Transactional
	public void deleteUserProfile(Integer userProfileId) {
        UserProfileEntity userProfile = (UserProfileEntity) sessionFactory.getCurrentSession().load(
        		UserProfileEntity.class, userProfileId);
        if (null != userProfile) {
            this.sessionFactory.getCurrentSession().delete(userProfile);
        }
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public UserProfileEntity getUserProfile(Integer userProfileId) {
				List<UserProfileEntity> users = this.sessionFactory.getCurrentSession()
						.createQuery("from UserProfileEntity where userId = ?")
						.setParameter(0, userProfileId).list();
			return users == null || users.size() <= 0 ? null : (UserProfileEntity) users.get(0);
	}
	
	@Override
	@Transactional
	public void updateProfile(UserProfileEntity userProfile) {
		UserProfileEntity profileToUpdate = getUserProfile(userProfile.getUserId());
		profileToUpdate.setFirstName(userProfile.getFirstName());
		profileToUpdate.setLastName(userProfile.getLastName());
		profileToUpdate.setCity(userProfile.getCity());
		profileToUpdate.setAddress(userProfile.getAddress());
		profileToUpdate.setPhoneNumber(userProfile.getPhoneNumber());
		
		this.sessionFactory.getCurrentSession().update(profileToUpdate);
	}

	@Override
	@Transactional
	public void addRating(RatingEntity rating) {
		 this.sessionFactory.getCurrentSession().save(rating);
		
	}

	@Override
	@Transactional
	public void updateRating(RatingEntity rating) {
		
		RatingEntity ratingToUpdate = getRating(rating.getUserId());
		ratingToUpdate.setTotal(rating.getTotal());
		this.sessionFactory.getCurrentSession().update(ratingToUpdate);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public RatingEntity getRating(Integer userId) {
		List<RatingEntity> rating = this.sessionFactory.getCurrentSession()
				.createQuery("from RatingEntity where userId = ?")
				.setParameter(0, userId).list();
	return rating == null || rating.size() <= 0 ? null : (RatingEntity) rating.get(0);
	}

	@Override
	@Transactional
	public void addComment(CommentEntity comment) {
		 this.sessionFactory.getCurrentSession().save(comment);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CommentEntity> getAllComments(Integer userId) {
		List<CommentEntity> comments = this.sessionFactory.getCurrentSession()
				.createQuery("from CommentEntity where userId = ? order by id DESC")
				.setParameter(0, userId).list();
		return comments;
	}
	
}
