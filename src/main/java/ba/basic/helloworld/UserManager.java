package ba.basic.helloworld;

import ba.basic.helloworld.dao.UserEntity;

public interface UserManager {
	public void addUser(UserEntity user);
    public void deleteUser(Integer userId);
    public UserEntity getUser(String email);
}
