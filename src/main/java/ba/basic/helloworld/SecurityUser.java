package ba.basic.helloworld;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import org.springframework.security.core.GrantedAuthority;

import ba.basic.helloworld.dao.UserEntity;

@Entity
public class SecurityUser extends org.springframework.security.core.userdetails.User{
	@ManyToOne
	private UserEntity user;

	public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public SecurityUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) throws IllegalArgumentException {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }
}
