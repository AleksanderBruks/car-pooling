package ba.basic.helloworld;

import java.util.List;

import ba.basic.helloworld.dao.CommentEntity;
import ba.basic.helloworld.dao.RatingEntity;
import ba.basic.helloworld.dao.UserProfileEntity;

public interface UserProfileManager {
    public void addUserProfile(UserProfileEntity userProfile);
    public List<UserProfileEntity> getAllProfiles();
    public void deleteUserProfile(Integer userProfileId);
    public UserProfileEntity getProfile(Integer userProfileId) ;
    public void updateProfile(UserProfileEntity userProfile);
    
    public void addRating(RatingEntity rating);
    public void updateRating(RatingEntity rating);
    public RatingEntity getRating(Integer userId);
    
    public void addComment(CommentEntity comment);
    public List<CommentEntity> getAllComments(Integer userId);
}
