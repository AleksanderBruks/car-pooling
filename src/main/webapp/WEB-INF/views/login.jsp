<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<html>
<head>
<jsp:include page="bootstrapheader.jsp"/>
</head>

    <body>
    	<div class="container">
    	<div class="span12 logoDiv"></div>
    	<div class="well span4">
        
        <form  name="f" action="<c:url value='j_spring_security_check'/>"
                    method="POST">
          <legend>Logowanie</legend>
          <div class="control-group">
            	
            	<div class="controls">
              		<input type="text" id="inputEmail" name="j_username" placeholder="Email">
          		</div>
          </div>
          <div class="control-group">
            	
            	<div class="controls">
              		<input type="password" id="inputPassword" name="j_password" placeholder="Haslo">
          		</div>
          </div>
          <div class="control-group">
          		<button type="submit" class="btn">Zaloguj</button>
          		<a class="btn" href="${pageContext.request.contextPath}/register">Rejestruj</a>
          </div>
        </form>
        </div>
        </div>
    </body>
</html>