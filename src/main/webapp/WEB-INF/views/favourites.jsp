<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Favourites</title>
<jsp:include page="bootstrapheader.jsp"/>
</head>
<body>
	<div class="container">
		<div class="span12 logoDiv"></div>
		<div class="navbar">
	      <div class="navbar-inner">
	        <a class="brand" href="${pageContext.request.contextPath}">Menu</a>
	        <ul class="nav">
	          <li class="active"><a href="${pageContext.request.contextPath}/logout">Wyloguj</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/announcement">New announcement</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/myProfile">My Profile</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/favourites">Favourites</a>
	          <li class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/myReserved">My reserved</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/regulations">Regulations</a></li>
	          </li>
	          <!-- <li><a href="#">Wyloguj</a></li> -->
	        </ul>
	      </div>
	    </div>

		<h2>Favourite drivers:</h2>
		<c:if  test="${!empty drivers}">
		<ul>
		<c:forEach items="${drivers}" var="driver">
    <a href="userProfile/${driver.value.getUserId()}">${driver.value.getFirstName()} ${driver.value.getLastName()} from ${driver.value.getCity()} </a>
    		<a href="deleteFavourite/${driver.key}">
    		<img src="${pageContext.request.contextPath}\resources\x.png" alt="delete"/></a><br>
		</c:forEach>
		</ul>
		</c:if>
		
		<h2>Favourite routes:</h2>
		<c:if  test="${!empty routes}">
		<table  class="table table-striped">
		<tr>
		    <th>From</th>
		    <th>To</th>
		</tr>
		<c:forEach items="${routes}" var="route">
		    <tr>
		        <td>${route.from}</td>
		        <td>${route.to}</td>
		        <td width="20px">    		<a href="deleteFavourite/${route.id}">
    		<img src="${pageContext.request.contextPath}\resources\x.png" alt="delete"/></a><br></td>
		    </tr>
		</c:forEach>
		</table>
		</c:if>
				<form:form method="post" action="addFavouriteRoute" commandName="newRoute">
		  
		    <table>
		    <tr>
		        <td><form:label path="from"><spring:message code="label.source"/></form:label></td>
		        <td><form:input path="from" /></td>
		    </tr>
		    <tr>
		        <td><form:label path="to"><spring:message code="label.destination"/></form:label></td>
		        <td><form:input path="to" /></td>
		    </tr>
		    <tr>
		        <td colspan="2">
		            <input type="submit" class="btn" value="<spring:message code="label.add"/>"/>
		        </td>
		    </tr>
		</table> 
		</form:form>

	</div>
</body>
</html>