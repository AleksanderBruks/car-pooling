<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Edycja listy dostepnych podrozy w bazie danych z wykorzystaniem hibernate</title>
	<jsp:include page="bootstrapheader.jsp"/>
</head>
<body>
	<div class="container">
		<div class="span12 logoDiv"></div>
		<div class="navbar">
	      <div class="navbar-inner">
	        <a class="brand" href="${pageContext.request.contextPath}">Menu</a>
	        <ul class="nav">
	          <li class="active"><a href="${pageContext.request.contextPath}/logout">Wyloguj</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/announcement">New announcement</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/myProfile">My Profile</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/favourites">Favourites</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/myReserved">My reserved</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/regulations">Regulations</a></li>
	          <!-- <li><a href="#">Wyloguj</a></li> -->
	        </ul>
	      </div>
	    </div>
	    <div class="main">
	    <div class="content"> 
		      
		<h3>Travels</h3>
		<c:choose>
		<c:when test="${!empty travelsList}">
		<table  class="table table-striped">
		<tr>
		    <th>Who</th>
		    <th>From</th>
		    <th>To</th>
		    <th>When</th>
		    <th>Free places</th>
		    <th>Cost</th>
		    <th>Akcja</th>
		</tr>
		<c:forEach items="${travelsList}" var="trav">
		    <tr>
		        <td><a href="${pageContext.request.contextPath}/userProfile/${trav.who}">${drivers.get(trav.who).getFirstName()} ${drivers.get(trav.who).getLastName()}</a></td>
		        <td>${trav.from}</td>
		        <td>${trav.to}</td>
		        <td>${trav.when}</td>
		        <td>${trav.freePlaces}</td>
		        <td>${trav.cost}</td>
		        <td><a href="${pageContext.request.contextPath}/addReserved/${trav.id}">Join in</a> </td>
		    </tr>
		</c:forEach>
		</table>
		</c:when>
		<c:otherwise>
		No items found!
		</c:otherwise>
		</c:choose>
		</div>
		<div class="quickNav">
		Quick search</br></br>
		
		<h5>Favourite drivers:</h5></br>
		<c:if  test="${!empty favouriteDrivers}">
		<c:forEach items="${favouriteDrivers}" var="driver">
    <a href="${pageContext.request.contextPath}/quickSearch/${driver.getKey()}">${driver.value.getFirstName()} ${driver.value.getLastName()}</a></br>
		</c:forEach>
		</c:if>
		
		<h5>Favourite routes:</h5></br>
		<c:if  test="${!empty routes}">
		<h6>From - To</h6>
		<c:forEach items="${routes}" var="route">
		<a href="${pageContext.request.contextPath}/quickSearch/${route.getId()}">${route.from} - ${route.to}</a></br>
		</c:forEach>
		</c:if>
		</div>	
		</div>
	</div>
  
</body>
</html>