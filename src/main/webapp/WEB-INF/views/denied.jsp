<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<jsp:include page="bootstrapheader.jsp"/>
</head>
    <body>
	     <div class="container">
	     	<div class="span12 logoDiv"></div>
	     	<div class="offset2 span8">
		        <h1 id="banner">Niepoprawne logowanie !!</h1>
		     
		        <hr />
		     
		        <c:if test="${not empty error}">
		            <div style="color:red">
		                Podano niepoprawne dane logowania !!<br />
		            </div>
		        </c:if>
		     
		        <p class="message">Brak dostepu!</p>
		        <a href="${pageContext.request.contextPath}/login">Wroc do strony logowania</a>
        	</div>
        </div>
    </body>
</html>