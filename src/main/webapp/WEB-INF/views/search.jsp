<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Search</title>
	<jsp:include page="bootstrapheader.jsp"/>
</head>
<body>
	<div class="container">
		<div class="span12 logoDiv"></div>
		<div class="navbar">
	      <div class="navbar-inner">
	        <a class="brand" href="${pageContext.request.contextPath}">Menu</a>
	        <ul class="nav">
	          <li class="active"><a href="${pageContext.request.contextPath}/logout">Wyloguj</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/announcement">New announcement</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/myProfile">My Profile</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/favourites">Favourites</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/myReserved">My reserved</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/regulations">Regulations</a></li>
	          <!-- <li><a href="#">Wyloguj</a></li> -->
	        </ul>
	      </div>
	    </div>
	    <div class="main">
	    <div class="content"> 
		<h2>Search for route:</h2>

		<form:form method="post" action="searchRoute" commandName="travel">
		    <table>
		    <tr>
		        <td><form:label path="from"><spring:message code="label.source"/></form:label></td>
		        <td><form:input path="from" required="true" /></td>
		    </tr>
		    <tr>
		        <td><form:label path="to"><spring:message code="label.destination"/></form:label></td>
		        <td><form:input path="to" required="true" /></td>
		    </tr>

		    <tr>
		        <td colspan="2">
		            <input type="submit" class="btn" value="<spring:message code="label.ok"/>"/>
		        </td>
		    </tr>
		</table> 
		</form:form>

		</div>	
		</div>
	</div>
  
</body>
</html>