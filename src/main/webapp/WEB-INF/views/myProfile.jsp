<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit your profile here</title>
<jsp:include page="bootstrapheader.jsp"/>
</head>
<body>
	<div class="container">
		<div class="span12 logoDiv"></div>
		<div class="navbar">
	      <div class="navbar-inner">
	        <a class="brand" href="${pageContext.request.contextPath}">Menu</a>
	        <ul class="nav">
	          <li class="active"><a href="${pageContext.request.contextPath}/logout">Wyloguj</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/announcement">New announcement</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/myProfile">My Profile</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/favourites">Favourites</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/myReserved">My reserved</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/regulations">Regulations</a></li>
	          <!-- <li><a href="#">Wyloguj</a></li> -->
	        </ul>
	      </div>
	    </div>
	    
		<h2>Edit your profile here:</h2>
		  
		<form:form method="post" action="addProfile" commandName="profile">
		  
		    <table>
		    <tr>
		        <td><form:hidden path="userId"></form:hidden></td>
		    </tr>
		    <tr>
		        <td><form:label path="firstName"><spring:message code="label.firstName"/></form:label></td>
		        <td><form:input path="firstName" /></td>
		        <td><form:label path="lastName"><spring:message code="label.lastName"/></form:label></td>
		        <td><form:input path="lastName" /></td>
		    </tr>
		    <tr>
		        <td><form:label path="city"><spring:message code="label.city"/></form:label></td>
		        <td><form:input path="city" /></td>
		        <td><form:label path="address"><spring:message code="label.address"/></form:label></td>
		        <td><form:input path="address" /></td>
		    </tr>
		    <tr>
		        <td><form:label path="phoneNumber"><spring:message code="label.phoneNumber"/></form:label></td>
		        <td><form:input path="phoneNumber" /></td>
		    </tr>
		    <tr>
		        <td colspan="2">
		            <input type="submit" class="btn" value="<spring:message code="label.ok"/>"/>
		        </td>
		    </tr>
		</table> 
		</form:form>
	<div class="main">
	<div class="content">
	<h3>Comments:</h3>
		<c:if  test="${!empty comments}">
		<table  class="table table-striped">
		<c:forEach items="${comments}" var="comm">
		    <tr>
		        <td>${comm.comment}</td>
		    </tr>
		</c:forEach>
		</table>
		</c:if>

	</div>
	<div class="quickNav">
	Total user rating: <b> ${rating.getTotal()}</b><br/>
	</div>
	</div>
	</div>
</body>
</html>