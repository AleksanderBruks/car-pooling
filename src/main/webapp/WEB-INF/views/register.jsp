<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<jsp:include page="bootstrapheader.jsp"/>
<script type="text/javascript">
</script>
</head>
<body>


	<div class="container">
    	<div class="span12 logoDiv"></div>
    	<div class="well span4">
        <div>
        <form:form  action="register" method="POST" commandName="userForm">
          <legend>Rejestracja</legend>
          <div class="control-group">
            	<div class="controls">
              		<form:input type="text" id="inputEmail" path="email" placeholder="E-mail"/>
          		</div>
          </div>
          <div class="control-group">
            	<div class="controls">
              		<form:input type="password" id="inputPassword" path="password" placeholder="Haslo"/>
          		</div>
          </div>
          <div class="control-group">
          		<button type="submit" class="btn">Zapisz</button> 
          		<a class="btn" href="${pageContext.request.contextPath}/login">Wroc</a>
          </div>
        </form:form>
        </div>
        </div>
    </div>

</body>
</html>