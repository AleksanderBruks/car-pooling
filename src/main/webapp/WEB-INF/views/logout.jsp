<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<% session.invalidate(); %>
<html>
<head>
    <title>Edycja listy dostepnych podrozy w bazie danych z wykorzystaniem hibernate</title>
	<jsp:include page="bootstrapheader.jsp"/>
</head>
    <body>
	     <div class="container">
	    	 <div class="span12 logoDiv"></div>
		     <div class="well offset3 span4">
		  	  	<legend>Wylogowano poprawnie!</legend>
		  		<a href="${pageContext.request.contextPath}/login">Powr�t</a>
	  	  </div>
        </div>
    </body>
</html>
