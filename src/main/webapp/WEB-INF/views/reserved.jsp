<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Zarezerwowane przejazdy</title>
	<jsp:include page="bootstrapheader.jsp"/>
</head>
<body>
	<div class="container">
		<div class="span12 logoDiv"></div>
		<div class="navbar">
	      <div class="navbar-inner">
	        <a class="brand" href="${pageContext.request.contextPath}">Menu</a>
	        <ul class="nav">
	          <li class="active"><a href="${pageContext.request.contextPath}/logout">Wyloguj</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/announcement">New announcement</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/myProfile">My Profile</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/favourites">Favourites</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/myReserved">My reserved</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/regulations">Regulations</a></li>
	          <!-- <li><a href="#">Wyloguj</a></li> -->
	        </ul>
	      </div>
	    </div>
	    <div class="main">
	    <div class="content"> 
		      
		<h3>Travels</h3>
		<c:choose>
		<c:when test="${!empty allReserved}">
		<table  class="table table-striped">
		<tr>
		    <th>Who</th>
		    <th>From</th>
		    <th>To</th>
		    <th>When</th>
		    <th>Cost</th>
		    <th>Contact Number</th>
		</tr>
		<c:forEach items="${allReserved}" var="res">
		    <tr>
		        <td><a href="userProfile/${drivers.get(res.id).getId()}">${drivers.get(res.id).getFirstName()} ${drivers.get(res.id).getLastName()}</a></td>
		        <td>${travels.get(res.id).getFrom()}</td>
		        <td>${travels.get(res.id).getTo()}</td>
		        <td>${travels.get(res.id).getWhen()}</td>
		        <td>${travels.get(res.id).getCost()}</td>
		        <td>${drivers.get(res.id).getPhoneNumber()}</td>
		    </tr>
		</c:forEach>
		</table>
		</c:when>
		<c:otherwise>
		No items found!
		</c:otherwise>
		</c:choose>
		</div>	
		</div>
	</div>
  
</body>
</html>