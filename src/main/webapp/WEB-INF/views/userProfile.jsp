<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User profile</title>
<jsp:include page="bootstrapheader.jsp"/>
</head>
<body>
	<div class="container">
		<div class="span12 logoDiv"></div>
		<div class="navbar">
	      <div class="navbar-inner">
	        <a class="brand" href="${pageContext.request.contextPath}">Menu</a>
	        <ul class="nav">
	          <li class="active"><a href="${pageContext.request.contextPath}/logout">Wyloguj</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/announcement">New announcement</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/myProfile">My Profile</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/favourites">Favourites</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
	          <li class="active"><a href="${pageContext.request.contextPath}/myReserved">My reserved</a></li>
	           <li class="active"><a href="${pageContext.request.contextPath}/regulations">Regulations</a></li>
	          <!-- <li><a href="#">Wyloguj</a></li> -->
	        </ul>
	      </div>
	    </div>
	    
		<h2>Profile:</h2>

		<table  class="table table-striped">
		<tr>
		    <th>First Name</th>
		    <th>Last Name</th>
		    <th>From</th>
		    <th width="20px">Add to favourites?</th>
		</tr>
		<tr>
		        <td>${profile.getFirstName()}</td>
		        <td>${profile.getLastName()}</td>
		        <td>${profile.getCity()}</td>
		        <td width="20px"><a href="addFavouriteDriver/${profile.getUserId()}"><img src="${pageContext.request.contextPath}\resources\ok1.png" alt="add"/></a></td>
		    </tr>
		    </table>
		    <button type="button" name="back" onclick="history.back()">Go back</button>

	<div class="main">
	<div class="content">
	<h3>Comments:</h3>
		<c:if  test="${!empty comments}">
		<table  class="table table-striped">
		<c:forEach items="${comments}" var="comm">
		    <tr>
		        <td>${comm.comment}</td>
		    </tr>
		</c:forEach>
		</table>
		</c:if>
				<form:form method="post" action="addComment" commandName="newComment">
		  <form:hidden path="userId"/>
		    <table>
		    <tr>
		        <td><form:label path="comment"><spring:message code="label.comment"/></form:label></td>
		        <td><form:textarea rows="4" cols="50" path="comment"/></td>
		    </tr>
		    <tr>
		        <td colspan="2">
		            <input type="submit" class="btn" value="<spring:message code="label.ok"/>"/>
		        </td>
		    </tr>
		</table> 
		</form:form>
	</div>
	<div class="quickNav">
	Total user rating: <b> ${rating.getTotal()}</b><br/>
	<a href="increaseRating/${profile.getUserId()}"><img src="${pageContext.request.contextPath}\resources\like.png" alt="add" width="70px" height="20px"/></a> &nbsp;
	<a href="decreaseRating/${profile.getUserId()}"><img src="${pageContext.request.contextPath}\resources\dislike.png" alt="add" width="70px" height="20px"/></a>
	</div>
	</div>
</div>
</body>
</html>